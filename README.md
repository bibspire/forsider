<img src=./icon.png width=96 height=96 align=right>

Værktøj til generering af forsider til biblioteksmaterialer, samt upload af disse.

Se [forsider.dk](https://forsider.dk) for detaljer.


# Udvikling

- `npm run dev` starter udviklingsserver (åben `localhost:8080` i seneste Chrome for udvikling). Bemærk adgang til filsystem kører kun i applikationen. Brug af opensearch kræver at man er på en DBC-whitelisted IP-adresse, og at cors er slået fra (`chrome --disable-web-security`).
- `npm prettier` indenterer koden
- `npm build` oversætter 

Koden ligger i `src/`, og er skrevet med react. Da det er en simpel applikation/prototype bruges `ReCom` redux wrapper. Komponenter der nedarver fra ReCom, har en `get` og `set` (async) metode hvor man kan hente og sætte data i redux tilstands-objektet direkte, givet en sti ned i objektet. Den består af følgende:

- React-komponenter
    - `Color.js` 
    - `CoverOptions.js`
    - `ExportSettings.js`
    - `ImageUpload.js`
    - `Main.js`
    - `Results.js`
    - `SearchCQL.js`
- `cover-html.js` Kode der generere forside-html der er grundlag for forside-billederne
- `index.js` entry-point for initialisering og hot reloading
- `render.js` genererer forside-billederen, og previews
- `search.js` kode for søgning, kalder opensearch eller den åbne platform.

## Application state

Overblik over applikationstilstanden

- `images[]`
    - `id` - id for image (120bit cryptograph hash of data url, - thus collision free with very high probability)
    - `name` - original file name
    - `url` - data url containing the image
- `search` 
    - `results`
    - `query`
    - `searching`
    - `error`
    - `page`
- `options[image-id]`
    - `background` - `{r,g,b,a}`
    - `font`
    - `fontScale`
    - `maxLen`
    - `yPos` 0-100
- `export`
    - `overwrite`
    - `overwriteOwn`
    - `singlePage`
    - `exporting`
    - `dirname`
- `currentImage`
- `query` search query which yields results
- `ui`
    - `currentResult`
    - `previewHtml`
    - `previewUrl`

# License

```
Copyright 2017-2020 Solsort ApS
Released as opensource under Mozilla Public License 2.0
```
