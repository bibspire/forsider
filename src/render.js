import { sleep } from "./util";
import { html2png, html2jpg } from "./html-to-canvas.js";
import { ReCom, store, set, get } from "./recom";
import coverHtml from "./cover-html";
import { search, updateCoverStatus } from "./search";
const { saveCover, saveRules } = require("./db");

function renderData(i) {
  let images = get("images", []);
  let results = get("search.results", []);
  let searchPage = get("search.page", 0);
  let image = images[(i + searchPage * 10) % images.length];
  let imageHash = image.id;
  let cfg = get(["options", imageHash], {});
  let meta = results[i];
  return { image, meta, cfg, imageHash };
}
async function renderSearchResult(i, width, height) {
  let { image, meta, cfg } = renderData(i);
  let html = coverHtml(image, meta, cfg, width, height);
  return await html2jpg(html, {
    deviceWidth: width,
    width: width,
    height: height,
  });
}

export async function generateCovers() {
  let writeFileSync, pathSep;
  if (window.require) {
    writeFileSync = window.require("fs").writeFileSync;
    pathSep = window.require("path").sep;
  } else {
    writeFileSync = () => {};
    pathSep = "/";
  }

  set("export.exporting", true);
  let ruleId;
  try {
    ruleId = await saveRules();
  } catch (e) {
    alert(
      "Fejl: kunne ikke gemme regler/billeder. Tjek at brugenavn/kodeord er korrekt indtastet, og at database-serveren kører."
    );
    set("export.exporting", false);
    return;
  }

  let errorCount = 0;
  let t = Date.now();
  do {
    let exportSettings = Object.assign({ singlePage: true }, get("export", {}));
    try {
      let results = get("search.results", []);
      console.log(
        "generating covers page ",
        get("search.page") + 1,
        "   deltaTime:",
        Date.now() - t
      );
      t = Date.now();
      if (get("images", []).length === 0 || results.length === 0) {
        set("export.exporting", false);
        return;
      }

      let promises = [];
      for (let i = 0; i < results.length; ++i) {
        await sleep();
        let meta = results[i];
        let pid = meta.pid[0].replace(/[^a-zA-Z0-9]/g, "_");
        /*
      console.log(
        "generate cover for",
        meta.pid[0],
        get("search.query"),
        get("search.page"),
        renderData(i).cfg,
        renderData(i).imageHash
      );
      */
        let filename =
          (exportSettings.dirname ? exportSettings.dirname + pathSep : "") +
          pid +
          ".jpg";

        if (
          (!meta.HAS_OWN_COVER &&
            meta.coverUrlThumbnail &&
            !exportSettings.overwrite) ||
          (meta.HAS_OWN_COVER && !exportSettings.overwriteOwn)
        ) {
          continue;
        }

        if (!get("export.exporting")) {
          return;
        }

        let dataUrl = await renderSearchResult(i, 1000 / 2, 1620 / 2);
        //let dataUrl = await renderSearchResult(i, 2 * 260, 2 * 420);

        if (!dataUrl.startsWith("data:image/jpeg;base64,")) {
          alert("error");
          throw new Error("encoding error");
        }

        let imageData = atob(dataUrl.slice(23));
        promises.push(
          saveCover(meta.pid[0], imageData, {
            rule: ruleId,
            query: get("search.query"),
            page: get("search.page"),
            pos: i,
            cfg: renderData(i).cfg,
            imageHash: renderData(i).imageHash,
          })
        );
        updateCoverStatus();
      }

      console.log("waiting for upload");
      await Promise.all(promises);
      console.log("uploaded");
    } catch (e) {
      console.log("error generating/saving images", e);
      ++errorCount;
    }
    if (errorCount > 20) {
      return alert(
        "Fejl: kunne ikke gemme regler/billeder. Tjek at brugernavn/kodeord er korrekt indtastet, og at der er forbindelse til database-serveren."
      );
      set("export.exporting", false);
    }
    if (errorCount) {
      continue;
    }
    errorCount = 0;

    if (exportSettings.singlePage) {
      set("export.exporting", false);
      return;
    }
    await search(get("search.query"), 1 + get("search.page", 0));
  } while (get("export.exporting"));
}

let previewRerun = false,
  previewRunning = false;

export async function renderPreviews() {
  if (previewRunning) {
    previewRerun = true;
    return;
  }
  previewRerun = false;
  previewRunning = true;

  try {
    let results = get("search.results", []);
    let previews;
    if (get("images", []).length > 0 && results.length > 0) {
      previews = get("previews", []);
      for (let i = 0; i < results.length; ++i) {
        previews[i] = previews[i] || {};
        previews[i].dataUrl = await renderSearchResult(i, 260, 420);
        await sleep();
      }
    } else {
      previews = [];
    }
    set("previews", previews);

    previewRunning = false;
    if (previewRerun) {
      setTimeout(renderPreviews, 0);
    }
  } catch (e) {
    previewRunning = false;
    if (previewRerun) {
      setTimeout(renderPreviews, 0);
    }
    throw e;
  }
}
