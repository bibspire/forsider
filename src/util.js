const sha = require("js-sha256");

function hash(str) {
  return btoa(
    String.fromCharCode.apply(String, sha.sha256.create().update(str).array())
  ).slice(0, 20);
}

exports.hash = hash;

exports.sleep = (time) =>
  new Promise((resolve, reject) => setTimeout(resolve, time || 0));

// ## str
//
exports.str = (o) => {
  try {
    return JSON.stringify(o, null, 2);
  } catch (e) {
    return String(o);
  }
};

// ## randomId
//
exports.randomId = () => Math.random().toString(36).slice(2, 12);

// ## file2url
//
exports.file2url = (f) =>
  new Promise(function (resolve) {
    var reader = new FileReader();
    reader.addEventListener("load", function () {
      resolve(reader.result);
    });
    reader.readAsDataURL(f);
  });

// ## loadImage
//
exports.loadImage = (src) =>
  new Promise(function (resolve, reject) {
    var img = new Image();
    img.src = src;
    img.onload = function () {
      resolve(img);
    };
    img.onerror = reject;
  });

// ## escapeXml
//
exports.escapeXml = (str) => {
  let result;
  //result = str.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/#/g, '&#35;')//.replace(/[^-;&a-zA-Z .()]/g, s => `&#${s.charCodeAt(0)};`);
  result = str.replace(/#/g, " ");
  result = `<![CDATA[${result}]]>`;
  console.log(result, str);
  return result;
};
//exports.escapeXml = (str) => str.replace(/&/g, "&amp;")

// # Inactive code
/*
function nosymb(str) {
  return str.replace(/[^a-zA-Z_0-9]/g, '').toLowerCase();
}

export function loadGoogleFont(font) {
  let id = 'GOOGLEFONT' + nosymb(font);
  if(document.getElementById(id)) {
    return;
  }
  let elem = document.createElement('link');
  Object.assign(elem, {
    rel: 'stylesheet',
    type: 'text/css',
    href: 'https://fonts.googleapis.com/css?family=' + font,
  });
  document.head.appendChild(elem);
}
*/
