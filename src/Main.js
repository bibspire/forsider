import React from "react";
import { ReCom, store, set, get } from "./recom";

import Paper from "material-ui/Paper";

import SearchCQL from "./SearchCQL";
import { updateCoverStatus } from "./search";
import Results from "./Results";
import CoverOptions from "./CoverOptions";
import { generateCovers, renderPreviews } from "./render";
import ExportSettings from "./ExportSettings";
import TextField from "material-ui/TextField";
import SelectField from "material-ui/SelectField";
import MenuItem from "material-ui/MenuItem";

set("openplatform.token", localStorage.getItem("openplatform.token") || "");
set(
  "openplatform.searchProfile",
  localStorage.getItem("openplatform.searchProfile") || ""
);
set(
  "webdav.url",
  localStorage.getItem("webdav.url") || "https://data.forsider.dk/test"
);
set("webdav.username", localStorage.getItem("webdav.username") || "ballerup");
set("webdav.password", localStorage.getItem("webdav.password") || "");

export default class Main extends ReCom {
  constructor(props, context) {
    super(props);
  }

  render() {
    //console.log('Store:', store.getState().toJS());

    let currentResult = this.get("ui.currentResult", 0);
    let currentPage = this.get("search.page", 0);
    let images = this.get("images", []);
    let currentImage = "";
    if (images.length > 0) {
      currentImage =
        images[(currentResult + 10 * currentPage) % images.length].id;
    }

    this.get(["options", currentImage]);
    renderPreviews();

    return (
      <div>
        <Paper style={{ margin: 10, padding: "0 10px 0 10px" }}>
          <SearchCQL />
          <Results />
        </Paper>

        <div style={{ display: "flex" }}>
          <div style={{ flex: "0 0 260px" }}>
            <ExportSettings />
            <Paper
              style={{
                display: "inline-block",
                margin: 10,
                width: 260,
              }}
            >
              <img src={this.get(["previews", currentResult, "dataUrl"])} />
            </Paper>
            <Paper style={{ margin: 10, padding: 10 }}>
              <h3>Openplatform konfiguration</h3>
              <TextField
                floatingLabelText="Token"
                value={this.get("openplatform.token")}
                onChange={(_, val) => {
                  localStorage.setItem("openplatform.token", val);
                  this.set("openplatform.token", val);
                }}
              />
              <paragraph>
                <small>
                  Token til openplatform kan genereres på{" "}
                  <a href="https://openplatform.dbc.dk">openplatform.dbc.dk</a>{" "}
                  udfra bibliotekets client-id/client-secret.
                </small>
              </paragraph>
              <TextField
                floatingLabelText="Søgeprofil"
                value={this.get("openplatform.searchProfile")}
                onChange={(_, val) => {
                  localStorage.setItem("openplatform.searchProfile", val);
                  this.set("openplatform.searchProfile", val);
                }}
              />
              <paragraph>
                <small>
                  Søgeprofil sendes med søgninger, og kan bruges til at tilpasse
                  resultatet. <em>Kan udelades.</em>
                </small>
              </paragraph>
            </Paper>
            <Paper style={{ margin: 10, padding: 10 }}>
              <h3>Forside-server</h3>
              <paragraph>
                <small>
                  Indstillinger for hvor de genererede forsider gemmes. Dette er
                  en "CORS-enabled WebDAV-server".
                </small>
              </paragraph>
              <TextField
                floatingLabelText="Url"
                value={this.get("webdav.url")}
                onChange={(_, val) => {
                  localStorage.setItem("webdav.url", val);
                  this.set("webdav.url", val);
                }}
              />
              <TextField
                floatingLabelText="Brugernavn"
                value={this.get("webdav.username")}
                onChange={(_, val) => {
                  localStorage.setItem("webdav.username", val);
                  this.set("webdav.username", val);
                }}
              />
              <TextField
                floatingLabelText="Kodeord"
                type="password"
                value={this.get("webdav.password")}
                onChange={(_, val) => {
                  localStorage.setItem("webdav.password", val);
                  this.set("webdav.password", val);
                }}
              />
            </Paper>
          </div>

          <Paper
            style={{
              flex: "1 1 auto",
              margin: 10,
              padding: 10,
            }}
          >
            <CoverOptions currentImage={currentImage} />
          </Paper>
        </div>
      </div>
    );
  }
}
