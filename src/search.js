import React from "react";
import { ReCom, set, get } from "./recom";
import { str, sleep } from "./util";
import _ from "lodash";

if(!window.OP_TOKEN) (async () => 
  window.OP_TOKEN = await (await fetch("get_op_token.txt")).text()
)();

export async function updateCoverStatus() {
  // TODO: check if cover exists in mycrud
  /*
    set(
      "search.results",
      get("search.results", []).map((o) =>
        Object.assign(o, {
          HAS_OWN_COVER: fsExistsSync(fileName(o.pid[0])),
        })
      )
    );
  */
}

export async function search(query, page, retry) {
  retry = retry || 0;
  page = Math.max(0, page | 0);

  set("search.results", []);
  set("search.page", page);
  set("search.query", query);
  set("search.searching", true);
  set("search.error", undefined);
  set("search.resultCount", undefined);

  let dbc = window.dbcOpenPlatform;
  try {
    let results;
    let token = (get("openplatform.token") || window.OP_TOKEN).trim();

    if (!dbc.connected()) { await dbc.connect(token); }

    let searchUrl = 'https://openplatform.dbc.dk/v3/search?access_token=' + token;
    searchUrl += "&q=" + encodeURIComponent(query);
    searchUrl += "&limit=" + 10;
    searchUrl += "&offset=" + page * 10;
    if(get("openplatform.searchProfile")) {
      searchUrl += "&profile=" + encodeURIComponent(get("openplatform.searchProfile"));
    }
    let {data, hitCount} = await (await fetch(searchUrl)).json()
    results = data;

    if (Array.isArray(results)) {
      results = results.map((o) =>
        Object.assign(o, {
          TITLE: o.dcTitle || o.dcTitleFull || o.title || [],
          CREATOR: o.dcCreator || o.creatorAut || o.creator || [],
        })
      );
    }
    if (!results || results.length === 0) {
      throw { error: "intet søgeresultat" };
    }

    try {
      let thumbs = await window.dbcOpenPlatform.work({
        pids: results.map((o) => o.pid[0]),
        fields: ["identifier", "coverUrlThumbnail"],
      });
      for (let i = 0; i < Math.min(thumbs.length, results.length); ++i) {
        results[i].coverUrlThumbnail = thumbs[i].coverUrlThumbnail;
      }
    } catch (e) {
      console.error("Error (openplatforn)", e);
    }

    set("search.results", results);
    set("search.resultCount", hitCount);

    // wait until results has been set
    await sleep();
    for (let i = 0; !_.isEqual(results, get("search.results")); ++i) {
      await sleep(10);
      if (i >= 100) {
        throw new Error("changes to results did not get through");
      }
    }
  } catch (e) {
    console.log(e);
    if (retry < 10) {
      return await search(query, page, retry + 1);
    }
    set("search.error", str(e));
  }
  await updateCoverStatus();
  set("search.searching", false);
}
