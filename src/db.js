let { ReCom, store, set, get } = require("./recom");
let { hash, sleep } = require("./util");

async function put(name, o) {
  let url = get('webdav.url');
  let headers = {
        "Content-Type": "application/binary",
        Authorization: "Basic " + btoa(get('webdav.username')+ ":" + get('webdav.password'))
  }
  return await fetch(`${url}/${name}`, {
      method: "PUT",
      headers,
      body: o,
    });
}

function bucketPath(s) {
  let bucket = s.trim().slice(-2);
  return 'covers/' + (bucket.match(/^[0-9][0-9]$/) ? bucket : 'other') + '/';
}

async function saveRules() {
  const images = get("images");
  for (const o of images) {
    await put('source/img_' + o.id + '.json', JSON.stringify(o));
  }
  const options = JSON.stringify(get("options"));
  let key = "rule_" + hash(options);
  put('source/rule_' + hash(options) + '.json', options)
  return key;
}
async function saveCover(pid, cover, rule) {
  let path = bucketPath(pid);
  await put(path + pid + '.json', JSON.stringify(rule));
  await put(path + pid + '.jpg', Uint8Array.from(cover.split("").map((c) => c.charCodeAt(0))));
}
  /*
setTimeout(async () => {
  // TODO - under development, check that writes to server works
  let username = get('webdav.username');
  let password = get('webdav.password');
  console.log('here',
    get('webdav.url'),
    get('webdav.username'),
    get('webdav.password'),
  );
  let text = (new Date()).toISOString();
  let result = await fetch(`${url}/hello.txt`, {
    method: "PUT",
    headers: {
      "Content-Type": "application/binary",
      Authorization: auth
    },
    body: Uint8Array.from(text.split("").map((c) => c.charCodeAt(0))),
  });

}, 200);
*/

module.exports = { saveCover, saveRules };
